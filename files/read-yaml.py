import yaml

# Tutorial: https://stackabuse.com/reading-and-writing-yaml-to-a-file-in-python/

with open(r'../resources/fruits.yaml') as f_fruits:
    # The FullLoader parameter handles the conversion from YAML
    # scalar values to Python the dictionary format
    fruits_list = yaml.load(f_fruits, Loader=yaml.FullLoader)

    print(fruits_list)
    # output:
    # {'apples': 20, 'mangoes': 2, 'bananas': 3, 'grapes': 100, 'pineapples': 1}

with open(r'../resources/categories.yaml') as f_categories:
    documents = yaml.full_load(f_categories)

    for item, doc in documents.items():
        print(item, ':', doc)
    
    # output:
    # sports : ['soccer', 'football', 'basketball', 'cricket', 'hockey', 'table tennis']
    # countries : ['Pakistan', 'USA', 'India', 'China', 'Germany', 'France', 'Spain']