import yaml

# Tuto: https://stackabuse.com/reading-and-writing-yaml-to-a-file-in-python/

dict_file = [
    {'sports' : [
        'soccer',
        'football'
    ]},
    {'countries' : [
        {'Russia': 'Moscow'},
        {'Germany': 'Berlin'},
        {'France': 'Paris'}
    ]},
    {'food' : {
        'breakfast' : 12,
        'lunch' : True,
        'snacks' : 'some apples'
    }}
]

with open(r'../resources/store.yaml', 'w') as f_store:
    documents = yaml.dump(dict_file, f_store)