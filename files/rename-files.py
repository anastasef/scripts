import os

# Purpose of this script:
# Rename several files from this pattern
# Neptune - Our Solar System - #8.mp4
# to this:
# 8-Neptune.mp4

PATH_TO_FILEDIR = "" # absolute path

# cd to the dirwith test files
os.chdir(PATH_TO_FILEDIR)

# check that we're in the needed dir
print('current dir:', os.getcwd())
print()

# Neptune - Our Solar System - #8.mp4 ->
# os.path.splitext(f) -> ('Saturn - Our Solar System - #7', '.mp4')
for f in os.listdir():
    file_name, f_ext = os.path.splitext(f)

    # print(file_name.split('-'))
    # ->['Saturn ', ' Our Solar System ', ' #7']

    
    f_title, f_course, f_num = file_name.split('-')

    f_title = f_title.strip()
    f_course = f_course.strip()
    f_num = f_num.strip()[1:].zfill(2)

    new_name = '{}-{}{}'.format(f_num, f_title, f_ext)

    os.rename(f, new_name)